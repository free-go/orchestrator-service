# Orchestrator

Contains all the parameters in order to run the services containers and add ready to use Traefik + MariaDB.

## Docker compose

```bash
# Run all the services in detached mode
docker-compose up -d

# Will remove all containers & stop the network
docker-compose down
```

## Traefik

Traefik dashboard : `localhost:8080`

## Services

* stock-service   : `localhost/stock   => :5000`
* product-service : `localhost/product => :5001`
* auth-service    : `localhost/auth    => :5002`
