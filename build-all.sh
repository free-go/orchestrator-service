#!/bin/sh

git pull

cd ../stock-service/
git pull
docker build -t stock-service .

cd ../auth-service/
git pull
docker build -t auth-service .

cd ../product-service/
git pull
docker build -t product-service .
